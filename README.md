# nfo

A tiny tool to manage a files bank by extension.
Usefull to cleanly read a large amount of inclusive contextualized  info files in a huge dir tree.

## Ex
Having 3 files to remind few cmd lines usage per OS. And wana hide occurence of specific OS.
```
/../../../remind_exploit_X.{win,osx,nix,nux,bsd...)
```
You just need to add/remove ext filters.

## Plugin
It can work as oh-my-zsh Plugin.
```
plugins=(... nfo)
```

