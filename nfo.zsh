

###################
# reset completion


__zt_unalias l
l()
{
	[[ ! -z ${nfo_sweeping+x} && $nfo_sweeping == true ]] && { nfo_show } ||
	{ ls++ --ptsf -latShc $@ }
}




#nfo_filter=("unx*" "bsd*")
nfo_rc_file=.nforc
nfo_sweeping=false
nfo_pattern_rootDir="\/self\/base\/os"
nfo_rootDir=

_nfo_wrapper()
{
	_pwd=$(pwd -P) # absolute pwd
	[[ $_pwd =~ $nfo_pattern_rootDir ]] &&
	{
		# rcfile ensuring being under true nfo dir's vault
		nfo_rootDir=$(sed "s#\(.*$nfo_pattern_rootDir\).*#\1#g" <<< $_pwd)
		[[ ! -r "$nfo_rootDir/$nfo_rc_file" ]] && { nfo_errhdlr 1 } ||
		{
			nfo_sweeping=true
		}
	} || nfo_sweeping=false
}

nfo_show()
{
	# only if vaulted and active filters
	_filter=($(nfo_filters list))
	[[ ${#_filter} -gt 1 ]] && 
	{ _filter=${(j:,:)_filter[@]}; _filter="{$_filter}" }

	[[ $nfo_sweeping = true && -n $_filter ]] &&
	{
		ls++ --ptsf -latShc --ignore="$_filter"
	}
}


nfo_help() { cat <<EOP
Usage: nfo [OPTION]...
  -h	This helper
  -i	Initialize nfo vault
  -a	Append rules
  -r	Remove rules
  -l	List active rules
  :     [+|-]<ext> short hand rule set
EOP
return 1;
}

nfo_opts()
{
	args=''
	while [[ $# != 0 ]]; do; shift;
	[[ "$1" =~ ^[-:].* ]] && break || args="$args $1"
	done
	echo "$args"
}


__zt_unfunc nfo
autoload -Uz _nfo
nfo()
{
	typeset -A nfo_stack_occ=()

	while [ "$1" ]; do case "$1" in
	# :+param | :-param
	:*) op="${1:1:1}"; occ="${1:2}";

		# ensure sanitized occurence
		[[ "$occ" =~ "^[[:alnum:]]+$" ]] && {
			case $op in
				[+-]) nfo_stack_occ+=("$occ" "$op") ;;
				*) nfo_errhdlr 2 $op
			esac;
		} || nfo_errhdlr 3 $1
		;;

	# -l[ist] param0 param1
	-l) nfo_showfilters=true ;;
	-[ar])
			[[ -z $2 ]] && {  nfo_errhdlr 2 $1; return 1 }
			typeset -A opm=([a]="+" [r]="-");
			op=" $opm[${1:1:1}] ";
			occ="${(pj:$op:)${=$(nfo_opts $@)}}$op";
			# getting serial form : (toto + titi + ...)
			tmp_occ=( "${(Q@)${(z@)occ}}" )
			nfo_stack_occ+=($tmp_occ)
			shift $((${#tmp_occ}/2)) ;;


	-h) echo "from opt"; nfo_help; break; return 1;;
	 ?) nfo_errhdlr 4 $1; return 1;;
	 *) echo "from unk"; nfo_help; break; return 1 ;;

#	*) [ ! -z "${unk++}" ] &&
#	{
#		echo "unk<$1> $unk"; # | last: ${last:0:1}"; fnd="$fnd${fnd:+ }$1"
#	} || nfo_help ;;

	esac; [[ "$#" -gt 0 ]] && shift; done


	[[ ! -z ${nfo_stack_occ+x} ]] &&
	{
		nfo_stack_occ="${(j: :)${(qkv@)nfo_stack_occ}}" # serialize
		nfo_filters merge $nfo_stack_occ >/dev/null
	}

	[[ ! -z ${nfo_showfilters+x} ]] &&
	{
		nfo_filters list
		unset nfo_showfilters
	}
}


nfo_filters()
{
	nforc="/mnt/store/dev0/r0/self/base/os/.nforc"
	local filters=($(awk -F'=' '/filters/{ print $2 }' $nforc))

	mode=$1; shift;
	case $mode in

	list)   echo $filters; return 0 ;;
	append) filters+=($1); ;;
	remove) filters=(${filters[@]/$1}) ;;
	merge) stack=( "${(Q@)${(z@)1}}" );

		for occ op in ${(kv)stack}; do
			case $op in
			+) filters=$(nfo_filters append "$occ*") ;;
			-) filters=$(nfo_filters remove "$occ*") ;;
			*) nfo_errhdlr 2 $op ;;
			esac;
		done;
	;;
	esac;

	# update changes
	filters=($(printf "%s\n" "${filters[@]}" | sort -u | tr '\n' ' '))
	sed -ri "/filters/s/=.*/=$filters/" $nforc

	# only self call output
	[[ $0 != $funcstack[-1] ]] && echo $filters;

}

typeset -A nfo_errlst
nfo_errlst[1]="No rc file "
nfo_errlst[2]="[%s] unknown filter operand on setting"
nfo_errlst[3]="[%s] unexpected entry; Filter must be under [:alnum:]"
nfo_errlst[4]="unknown half option: %s"
nfo_errlst[5]="[%s] Missing argument..."
nfo_errhdlr()
{
	err=$1; shift
	printf $nfo_errlst[$err] $@
	return 1;
}


# load add-zsh-hook if it's not available yet
(( $+functions[add-zsh-hook] )) || autoload -Uz add-zsh-hook

# hook nfo onto <chpwd> function to detect activation pwd point
add-zsh-hook chpwd _nfo_wrapper

#print -- $funcfiletrace[1] $funcstack[-1]
#print -l -- $funcfiletrace - $funcsourcetrace - $funcstack - $functrace

